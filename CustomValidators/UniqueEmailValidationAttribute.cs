using System.ComponentModel.DataAnnotations;
using Companies.API.Repositories.Dapper;
using Companies.API.Repositories.Interfaces;

namespace Companies.API.CustomValidators
{
    public class UniqueEmailValidationAttribute: ValidationAttribute
    {

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            IEmployeesRepository repository = (IEmployeesRepository) validationContext.GetService(typeof(IEmployeesRepository));
            bool isEmailTaken = repository.IsEmailTaken(value as string);
            if (isEmailTaken)
            {
                string email = value as string;
                return new ValidationResult($"email {email} is taken.");
            }
            return ValidationResult.Success;
        }
    }
}