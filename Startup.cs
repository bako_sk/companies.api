﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Companies.API.ActionFilters;
using Companies.API.CustomMiddleware;
using Companies.API.Repositories.Dapper;
using Companies.API.Repositories.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Companies.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            // db
            // scoped - lives for entire scope - in this case request
            services.AddScoped<IDbConnection>((serviceProvider) => 
                new SqlConnection(Configuration.GetSection("ConnectionStrings")["DefaultConnection"]));
            // begin transaction for every request
            services.AddScoped<IDbTransaction>((serviceProvider) =>
            {
                var connection = serviceProvider.GetService<IDbConnection>();
                connection.Open();

                return connection.BeginTransaction();
            });
            // add action filter - every request will run in transaction, if no exception, commit, else it will rollback
            services.AddScoped(typeof(UnitOfWorkFilter), typeof(UnitOfWorkFilter));
            services.AddMvc(setup => { setup.Filters.AddService<UnitOfWorkFilter>(1); });
            
            
            // add repositories
            DapperCustomMapper.RegisertCustomMappings();
            services.AddScoped<ICompaniesRepository, DapperCompaniesRepository>();
            services.AddScoped<IDivisionsRepository, DapperDivisionsRepository>();
            services.AddScoped<IProjectRepository, DapperProjectsRepository>();
            services.AddScoped<IDepartmentsRepository, DapperDepartmentsRepository>();
            services.AddScoped<IEmployeesRepository, DapperEmployeesRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseMiddleware<ExceptionMiddleware>();
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                //HSTS Middleware (UseHsts) to send HTTP Strict Transport Security Protocol (HSTS) headers to clients.
                //app.UseHsts();
            }

            // allow cros-origin requests. CORS - Cros Origin Resource Sharing
            // just for localhost development.this setting is not good for production!!!!
            app.UseCors(x => x.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
	    // do not redirect to https
            //app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
