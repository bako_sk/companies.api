using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace Companies.API.CustomMiddleware
{
    public class ExceptionMiddleware
    {
        // ??? logger
        private readonly RequestDelegate _next;

        public ExceptionMiddleware(RequestDelegate next)
        {
            _next = next;
        }
        
        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(httpContext, ex);
            }
        }
 
        private Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            //!!! very basic
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
 
            // add error class?
            return context.Response.WriteAsync(new ErrorResponse() 
            {
                StatusCode = context.Response.StatusCode,
                // do not display exc message to client
                Message = "Internal Server Error from the custom middleware."
            }.ToString());
        }
    }

}