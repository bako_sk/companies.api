using System.Reflection.Metadata.Ecma335;
using Newtonsoft.Json;

namespace Companies.API.CustomMiddleware
{
    public class ErrorResponse
    {
        public string Message { get; set; }
        public int StatusCode { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
   
    
}