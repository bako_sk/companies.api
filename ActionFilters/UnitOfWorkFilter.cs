using System;
using System.Data;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Companies.API.ActionFilters
{
    public class UnitOfWorkFilter : IAsyncActionFilter
    {
        private readonly IDbTransaction _transaction;

        public UnitOfWorkFilter(IDbTransaction transaction)
        {
            _transaction = transaction;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var connection = _transaction.Connection;
            // before controller actions, checks for open db connection, if none, fail
            if (connection.State != ConnectionState.Open)
            {
                throw new NotSupportedException("Provided db connection was not open!");

            }
            // execute controller actions
            var executedContext = await next.Invoke();
            // if everything ok, commit transaction
            if (executedContext.Exception == null)
            {
                _transaction.Commit();
            }
            else
            {
                _transaction.Rollback();
            }

        }
    }
}