use kros_companies;
CREATE TABLE departments
(
  ID INT IDENTITY (1,1) PRIMARY KEY,
  Name      TEXT NOT NULL,
  Code      NVARCHAR(64) NOT NULL,
  ProjectID INT NOT NULL FOREIGN KEY references projects(ID),
  HeadID    INT NULL FOREIGN KEY references employees(EmployeeID) ON DELETE SET NULL 
);
GO
