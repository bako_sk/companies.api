USE kros_companies;
CREATE TABLE departments_employees
(
  DepartmentEmployeID INT IDENTITY (1,1) PRIMARY KEY,
  DepartmentID INT NOT NULL FOREIGN KEY references departments(ID) ON DELETE CASCADE ,
  EmployeeID INT NOT NULL FOREIGN KEY references employees(EmployeeID) ON DELETE CASCADE
);
GO
