use kros_companies;
CREATE TABLE projects
(
  ID INT IDENTITY (1,1) PRIMARY KEY,
  Name      TEXT NOT NULL,
  Code      NVARCHAR(64) NOT NULL,
  DivisionID INT NOT NULL FOREIGN KEY references divisions(ID),
  HeadID    INT NULL FOREIGN KEY references employees(EmployeeID) ON DELETE SET NULL 
);
GO
