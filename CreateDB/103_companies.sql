use kros_companies;
CREATE TABLE companies
(
  ID INT IDENTITY (1,1) PRIMARY KEY,
  Name      TEXT NOT NULL,
  Code      NVARCHAR(64) NOT NULL,
  HeadID    INT NULL FOREIGN KEY references employees(EmployeeID) ON DELETE SET NULL 
);
GO