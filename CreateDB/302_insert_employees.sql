USE kros_companies;
-- bosss
INSERT INTO employees(Degree, FirstName, LastName, PhoneExt, Email, RoleID) 
            VALUES ('Ing', 'Jozef', 'Ceo', '007', 'ceo@google.sk', 1);
-- division bosses
INSERT INTO employees(FirstName, LastName, PhoneExt, Email, RoleID) 
            VALUES ('Anna', 'Pekna', '017', 'pekna@google.sk', 2);
            
INSERT INTO employees(FirstName, LastName, PhoneExt, Email, RoleID)
VALUES ('Ivan', 'Hrozny', '018', 'hrozny@google.sk', 2);
INSERT INTO employees(FirstName, LastName, PhoneExt, Email, RoleID)
VALUES ('Jozefa', 'Dobrovolna', '019', 'dobrovolna@company.sk', 2);
-- project bosses
INSERT INTO employees(FirstName, LastName, PhoneExt, Email, RoleID)
VALUES ('Anna', 'Blizka', '027', 'blizka@google.sk', 3);
INSERT INTO employees(Degree, FirstName, LastName, PhoneExt, Email, RoleID)
VALUES ('Ing','Vincent', 'Vega', '028', 'vega@company.sk', 3);
INSERT INTO employees(FirstName, LastName, PhoneExt, Email, RoleID)
VALUES ('Arwen', 'Elrond', '029', 'elrond@google.sk', 3);
INSERT INTO employees(FirstName, LastName, PhoneExt, Email, RoleID)
VALUES ('Frodo', 'Baggins', '030', 'baggins@company.sk', 3);
INSERT INTO employees(Degree, FirstName, LastName, PhoneExt, Email, RoleID)
VALUES ('Phd', 'Red', 'Foreman', '031', 'foreman@google.sk', 3);
-- department bosses
INSERT INTO employees(FirstName, LastName, PhoneExt, Email, RoleID)
VALUES ('Jaromir', 'Jagr', '041', 'jagr@company.sk', 4);
INSERT INTO employees(Degree, FirstName, LastName, PhoneExt, Email, RoleID)
VALUES ('Ing','Jana', 'Jasna', '042', 'jasna@company.sk', 4);
INSERT INTO employees(Degree, FirstName, LastName, PhoneExt, Email, RoleID)
VALUES ('bc','Jana', 'Boss', '043', 'boss@company.sk', 4);
--employees 
INSERT INTO employees(FirstName, LastName, PhoneExt, Email, RoleID)
VALUES ('Jaroslav', 'Jarny', '051', 'jarny@company.sk', 5);
INSERT INTO employees(Degree, FirstName, LastName, PhoneExt, Email, RoleID)
VALUES ('Ing','Aragorn', 'Elesar', '052', 'elesar@company.sk', 5);
INSERT INTO employees(Degree, FirstName, LastName, PhoneExt, Email, RoleID)
VALUES ('bc','Juraj', 'Janosik', '053', 'janosik@company.sk', 5);
INSERT INTO employees(FirstName, LastName, PhoneExt, Email, RoleID)
VALUES ('Don', 'Corleone', '054', 'corleone@company.sk', 5);
INSERT INTO employees(Degree, FirstName, LastName, PhoneExt, Email, RoleID)
VALUES ('Ing','Robert', 'Hranol', '055', 'hranol@company.sk', 5);
INSERT INTO employees(Degree, FirstName, LastName, PhoneExt, Email, RoleID)
VALUES ('bc','Andrej', 'Kolesik', '056', 'kolesik@company.sk', 5);
GO
