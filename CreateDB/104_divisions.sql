use kros_companies;
CREATE TABLE divisions
(
  ID        INT IDENTITY (1,1) PRIMARY KEY,
  Name      TEXT NOT NULL,
  Code      NVARCHAR(64) NOT NULL,
  CompanyID INT NOT NULL FOREIGN KEY references companies(ID) ON DELETE CASCADE,
  HeadID    INT NULL FOREIGN KEY references employees(EmployeeID) ON DELETE SET NULL 
);
GO
