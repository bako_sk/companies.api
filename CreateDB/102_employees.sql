use kros_companies;
CREATE TABLE employees
(
  EmployeeID INT IDENTITY(1,1) PRIMARY KEY,
  Degree NTEXT NULL,
  FirstName NTEXT NOT NULL,
  LastName NTEXT NOT NULL,
  PhoneExt NVARCHAR(5) NULL,
  Email NVARCHAR(256) NOT NULL UNIQUE,
  RoleID INT NOT NULL FOREIGN KEY references roles(RoleID),
);
GO