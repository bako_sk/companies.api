USE kros_companies;
CREATE TABLE roles 
(
 RoleID  int IDENTITY(1,1) PRIMARY KEY,
 Name nvarchar(128) NOT NULL UNIQUE,
);
GO
