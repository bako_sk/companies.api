using System.ComponentModel.DataAnnotations;

namespace Companies.API.InputDTOs
{
    public class UpdateEmployeeDTO
    {
        [Required]
        public int EmployeeID { get; set; }
        [Required]
        public EmployeeDto EmployeeDto { get; set; }
    }
}