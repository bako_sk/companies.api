using System;
using System.ComponentModel.DataAnnotations;
using System.Data;
using Companies.API.CustomValidators;

namespace Companies.API.InputDTOs
{
    public class EmployeeDto
    {
        
        public string Degree { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string PhoneExt { get; set; }
        [Required]
        [EmailAddress]
        [UniqueEmailValidation]
        public string Email { get; set; }
        [Required]
        public string RoleName { get; set; }
    }
}