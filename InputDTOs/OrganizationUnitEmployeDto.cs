namespace Companies.API.InputDTOs
{
    public class OrganizationUnitEmployeDto
    {
        public EmployeeDto Employee { get; set; }
        public int unitId { get; set; }
    }
}