namespace Companies.API
{
    public class EmployeeRolesName
    {
       public const string CEO = "CEO"; 
       public const string DIVISON_HEAD = "DIVISION_HEAD"; 
       public const string PROJECT_HEAD = "PROJECT_HEAD"; 
       public const string DEPARTMENT_HEAD = "DEPARTMENT_HEAD"; 
       public const string DEPARTMENT_EMPLOYEE = "DEPARTMENT_EMPLOYEE"; 
    }
}