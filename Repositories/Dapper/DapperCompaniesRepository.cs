using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Companies.API.InputDTOs;
using Companies.API.Models;
using Companies.API.Repositories.Interfaces;
using Dapper;

namespace Companies.API.Repositories.Dapper
{
    public class DapperCompaniesRepository : DapperOrganizationUnitRepository, ICompaniesRepository
    {
        // for if transaction is open, it must be sent to query func for sqlexpres adapter
        public  DapperCompaniesRepository(
            IDbConnection connection,
            IDbTransaction transaction,
            IEmployeesRepository employeesRepository): base(connection, transaction, employeesRepository)
        {
        }

        public async Task<List<Company>> GetCompaniesWithoutDivisions()
        {
            string sql = @"
SELECT *
  FROM companies AS c
  JOIN employees AS e
    ON e.EmployeeID = c.HeadID
   ";
            var companies = await _connection.QueryAsync<Company, Employee, Company>(
                sql,
                (Company company, Employee employee) =>
                {
                    company.Head = employee;
                    return company;
                },
                null, 
                _transaction,
                splitOn: "EmployeeID" );
            return companies.ToList<Company>();
        }

        public async Task<Company> GetCompany(int id)
        {
            return await this.GetOrganizationUnit<Company>(id, "companies");
        }
       
        

        public async Task<Employee> AddCeo(OrganizationUnitEmployeDto employee)
        {
            return await this.AddUnitHead(employee.unitId, employee.Employee, "companies");
        }
    }
}