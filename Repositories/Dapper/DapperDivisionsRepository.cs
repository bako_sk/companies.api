using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Companies.API.InputDTOs;
using Companies.API.Models;
using Companies.API.Repositories.Interfaces;
using Dapper;

namespace Companies.API.Repositories.Dapper
{
    public class DapperDivisionsRepository : DapperOrganizationUnitRepository, IDivisionsRepository
    {
        public DapperDivisionsRepository(
            IDbConnection connection,
            IDbTransaction transaction,
            IEmployeesRepository employeesRepository) : base(connection, transaction, employeesRepository)
        {
        }


        public async Task<Division> GetDivision(int id)
        {
            return await this.GetOrganizationUnit<Division>(id, "divisions");
        }

        public async Task<Employee> AddDivisonHead(OrganizationUnitEmployeDto employee)
        {
            return await this.AddUnitHead(employee.unitId, employee.Employee, "divisions");
        }
    }
}