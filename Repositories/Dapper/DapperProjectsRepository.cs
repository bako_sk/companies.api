using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Companies.API.InputDTOs;
using Companies.API.Models;
using Companies.API.Repositories.Interfaces;
using Dapper;

namespace Companies.API.Repositories.Dapper
{
    public class DapperProjectsRepository : DapperOrganizationUnitRepository, IProjectRepository
    {

        public DapperProjectsRepository(
            IDbConnection connection,
            IDbTransaction transaction,
            IEmployeesRepository employeesRepository) : base(connection, transaction, employeesRepository)
        {
        }

        public async Task<Project> GetProject(int projectId)
        {
            return await this.GetOrganizationUnit<Project>(projectId, "projects");
        }

        public async Task<Employee> AddProjectHead(OrganizationUnitEmployeDto employee)
        {
            return await this.AddUnitHead(employee.unitId, employee.Employee, "projects");
        }
    }
}