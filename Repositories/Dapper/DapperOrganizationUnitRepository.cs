using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Companies.API.InputDTOs;
using Companies.API.Models;
using Companies.API.Repositories.Interfaces;
using Dapper;
using Microsoft.EntityFrameworkCore.Internal;

namespace Companies.API.Repositories.Dapper
{
    public abstract class DapperOrganizationUnitRepository
    {
        protected IDbConnection _connection;
        protected IDbTransaction _transaction;
        private IEmployeesRepository _employeesRepository;


        protected DapperOrganizationUnitRepository(IDbConnection connection, IDbTransaction transaction, IEmployeesRepository employeesRepository)
        {
            _connection = connection;
            _transaction = transaction;
            _employeesRepository = employeesRepository;
        }

        protected async Task<Employee> AddUnitHead(int id, EmployeeDto employee, string tableName)
        {
            Employee newEmployee = await _employeesRepository.AddEmployee(employee);
            if (newEmployee != null)
            {
                bool inserted = await this.AddEmployeeAsUnitHead(id, newEmployee.EmployeeID, tableName);
                if (!inserted)
                {
                    newEmployee = null;
                }
            }

            return newEmployee;
        }
        private async Task<bool> AddEmployeeAsUnitHead(int id, int employeeId, string tableName)
        {
            
            // check if tablename is valid tablename in db??
            // ugly hack - faster than checking DB
            if (! Helpers.IsTableNameCorrectUnitTable(tableName))
            {
                throw new Exception("Invalid table name from unit head update: " + tableName);
            }
            string sql = $"UPDATE {tableName} SET HeadID = @EmployeeID WHERE ID = @ID";
            int affectedRows = await _connection.ExecuteAsync(sql, new
            {
                ID = id,
                EmployeeID = employeeId
            }, _transaction);

            return affectedRows > 0;
        }
        protected async Task<T> GetOrganizationUnit<T>(int id, string tableName) where T : OrganizationUnit
        {
            if (! Helpers.IsTableNameCorrectUnitTable(tableName))
            {
                throw new Exception("Invalid table name from unit head update: " + tableName);
            }

            string join = this.GetJoinForTable(tableName);
            string sql = @"
SELECT u.ID,
       u.Name,
       u.Code,
       e.EmployeeID,
       e.Degree,
       e.FirstName,
       e.LastName,
       e.PhoneExt,
       e.Email,
       r.RoleID,
       r.Name,
       sub_unit.ID,
       sub_unit.Name,
       sub_unit.Code" +
  $" FROM {tableName} AS u" +
   @" LEFT OUTER  JOIN employees AS e
    ON e.EmployeeID = u.HeadID
LEFT OUTER  JOIN roles AS r
    ON r.RoleID = e.RoleID " +
  join +
 " WHERE u.ID = @ID;";
            var unitDictionary = new Dictionary<int, T>();
            var units = await _connection.QueryAsync<
                T,
                Employee,
                EmployeeRole,
                OrganizationUnit,
                T>(
                sql,
                (T unit, Employee employee, EmployeeRole role, OrganizationUnit subUnit) =>
                {
                    T unitEntry;
                    if (!unitDictionary.TryGetValue(unit.ID, out unitEntry))
                    {
                        unitEntry = unit;
                        if (employee != null)
                        {
                            employee.Role = role;
                            unitEntry.Head = employee;
                        }

                        unitDictionary.Add(unitEntry.ID, unitEntry);
                    }

                    if (subUnit != null)
                    {
                        unit.AddSubUnit(subUnit);
                    }

                    return unitEntry;
                    
                },
                new 
                {
                    Id = id,
                }, 
                _transaction,
                splitOn: "EmployeeID, RoleID, ID" );
            T unitFound = units.FirstOrDefault();

            return unitFound;
        }

        private string GetJoinForTable(string tableName)
        {
            string sql = "";
            switch (tableName)
            {
                case "companies":
                {
                    sql = @"
 LEFT OUTER JOIN divisions AS sub_unit
             ON sub_unit.CompanyID = u.ID ";
                    break;
                }
                case "divisions":
                {
                    sql = @"
LEFT OUTER JOIN projects AS sub_unit
             ON sub_unit.DivisionID = u.ID";
                    break;
                }
                case "projects":
                {
                    sql = @"
LEFT OUTER JOIN departments AS sub_unit
             ON sub_unit.ProjectID = u.ID";
                    break;
                }
            }
                    return sql;

        }
    }
}