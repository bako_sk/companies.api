using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlTypes;
using System.Linq;
using System.Threading.Tasks;
using Companies.API.InputDTOs;
using Companies.API.Models;
using Companies.API.Repositories.Interfaces;
using Dapper;

namespace Companies.API.Repositories.Dapper
{
    public class DapperEmployeesRepository : IEmployeesRepository
    {
        private IDbConnection _connection;
        private IDbTransaction _transaction;

        public DapperEmployeesRepository(IDbConnection connection, IDbTransaction transaction)
        {
            _connection = connection;
            _transaction = transaction;
        }

        public async Task<List<EmployeeRole>> GetAllRoles()
        {
            string sql = @"
SELECT *
  FROM roles";
            var roles = await _connection.QueryAsync<EmployeeRole>(sql,null, _transaction );
            return roles.ToList();
        }

        private async Task<EmployeeRole> GetRoleByName(string roleName)
        {
            
            string sql = @"
SELECT *
  FROM roles
 WHERE name = @RoleName";
            EmployeeRole role = await _connection.QueryFirstOrDefaultAsync<EmployeeRole>(sql,new {RoleName = roleName}, _transaction );
            return role;
        }

        public async Task<EmployeeRole> GetEmployeeRole(int employeeId)
        {
            string sql = @"
SELECT r.RoleID,
       r.Name
  FROM employees as e
  JOIN roles as r
    ON e.RoleID = r.RoleID
 WHERE  e.EmployeeID = @EmployeID";
            EmployeeRole role = await _connection.QueryFirstOrDefaultAsync<EmployeeRole>(
                sql, 
                new {EmployeID = employeeId},
                _transaction);
            return role;
        }


        public async Task<DepartmentEmployee> GetDepartmentEmployee(int employeeId)
        {
            string sql = @"
SELECT e.EmployeeID,
       e.Degree,
       e.FirstName,
       e.LastName,
       e.PhoneExt,
       e.Email,
       r.RoleID,
       r.Name,
       dep.ID,
       dep.Name,
       Dep.Code,
       p.ID,
       p.Name,
       p.Code,
       div.ID,
       div.Name,
       div.Code,
       c.ID,
       c.Name,
       c.Code
  FROM employees AS e
  JOIN roles AS r
    ON r.RoleID = e.RoleID
  LEFT OUTER JOIN departments_employees AS de 
    ON e.EmployeeID = de.EmployeeID
  LEFT OUTER JOIN departments AS dep
    ON de.DepartmentID = dep.ID
  LEFT OUTER JOIN projects AS p 
    ON dep.ProjectID = p.ID
  LEFT OUTER JOIN divisions AS div
    ON p.DivisionID = div.ID
  LEFT OUTER JOIN companies AS c 
    ON div.CompanyID = c.ID
 WHERE e.EmployeeID = @EmployeeID";
            
            Dictionary<int, DepartmentEmployee>  employeesDictionary = new Dictionary<int, DepartmentEmployee>();

               var employees = await _connection.QueryAsync<DepartmentEmployee, EmployeeRole, Department, Project, Division, Company, DepartmentEmployee>(
                    sql,
                    (
                        DepartmentEmployee employee,
                        EmployeeRole role,
                        Department department,
                        Project project,
                        Division division,
                        Company company) =>
                    {
                        
                         DepartmentEmployee employeeEntry;
                         if (! employeesDictionary.TryGetValue(employee.EmployeeID, out employeeEntry))
                         {
                             employeeEntry = employee;
                             employeeEntry.Role = role;
                             employeeEntry.Departments = new List<Department>();
                             employeesDictionary.Add(employeeEntry.EmployeeID, employeeEntry);
                         }

                         // check for null because of left outer join
                         if (department != null)
                         {
                             division.Company = company;
                             project.Division = division;
                             department.Project = project;
                             employeeEntry.Departments.Add(department);
                         }

                         return employeeEntry;
                    },
                    new
                    {
                        EmployeeID = employeeId,
                    },
                    _transaction,
                    splitOn:"EmployeeID, RoleID, ID, ID, ID, ID");

               return employees.FirstOrDefault();
        }

        public async Task<bool> UpdateEmployee(UpdateEmployeeDTO updatedEmployee)
        {
            EmployeeRole role = await this.GetRoleByName(updatedEmployee.EmployeeDto.RoleName);
            if (role == null)
            {
                throw new Exception("Update employee: Role with name " 
                                    + updatedEmployee.EmployeeDto.RoleName 
                                    + " not found.");
            }
            // trigger would be better
            string sql = @"
UPDATE employees
   SET Degree = ISNULL(@Degree, Degree),
       FirstName = ISNULL(@FirstName, FirstName),
       LastName = ISNULL(@LastName, LastName),
       PhoneExt = ISNULL(@PhoneExt, PhoneExt),
       Email = ISNULL(@Email, Email),
       RoleID = ISNULL(@RoleID, RoleID)
 WHERE EmployeeId = @EmployeeID
   AND(
        CONVERT(VARCHAR, Degree) != @Degree 
     OR CONVERT(VARCHAR, FirstName) != @FirstName 
     OR CONVERT(VARCHAR, LastName) != @LastName 
     OR CONVERT(VARCHAR, PhoneExt) != @PhoneExt 
     OR CONVERT(VARCHAR, Email) != @Email
     OR CONVERT(VARCHAR, RoleID) != @RoleID)";
     
            int affectedRows = await _connection.ExecuteAsync(sql, new {
                EmployeeID = updatedEmployee.EmployeeID,
                Degree = updatedEmployee.EmployeeDto.Degree,
                FirstName = updatedEmployee.EmployeeDto.FirstName,
                LastName = updatedEmployee.EmployeeDto.LastName,
                PhoneExt = updatedEmployee.EmployeeDto.PhoneExt,
                Email = updatedEmployee.EmployeeDto.Email,
                RoleID = role.RoleId
            }, _transaction);
            return affectedRows > 0;
        }

        public async Task<Employee> AddEmployee(EmployeeDto employee)
        {
            EmployeeRole role = await this.GetRoleByName(employee.RoleName);
            if (role == null)
            {
                throw new Exception("add employee: Role with name " 
                                    + employee.RoleName 
                                    + " not found.");
            }
            string sql = @"
INSERT INTO employees (Degree, FirstName, LastName, PhoneExt, Email, RoleID)
     OUTPUT INSERTED.*    
     VALUES (@Degree, @FirstName, @LastName, @PhoneExt, @Email, @RoleID)";
            Employee inserted = await  _connection.QuerySingleOrDefaultAsync<Employee>(sql, new
            {
                Degree = employee.Degree,
                FirstName = employee.FirstName,
                LastName = employee.LastName,
                PhoneExt = employee.PhoneExt,
                Email = employee.Email,
                RoleID = role.RoleId
            }, _transaction);
            if (inserted != null)
            {
                inserted.Role = role;
            }

            return inserted;
        }

        public async Task<bool> RemoveEmployee(int employeeId)
        {
            string sql = @"DELETE FROM employees WHERE EmployeeID = @EmployeeID";

            int affectedRows = await  _connection.ExecuteAsync(sql, new {EmployeeID = employeeId}, _transaction);
            return affectedRows > 0;
        }

        public async Task<OrganizationHead> GetOrganizationHead(int employeeId, string tableName)
        {
            if (! Helpers.IsTableNameCorrectUnitTable(tableName))
            {
                throw new Exception("Invalid table name from GetOrganizationHead: " + tableName);
            }
            string sql = "SELECT " +
                         "e.EmployeeID," +
                         "e.Degree," +
                         "e.FirstName," +
                         "e.LastName," +
                         "e.PhoneExt," +
                         "e.Email," +
                         "r.RoleID," +
                         "r.Name," +
                         "o.ID," +
                         "o.Name," +
                         "o.Code " +
                         "FROM employees AS e " +
                         "JOIN roles AS r ON r.RoleID = e.RoleID" +
                       $" LEFT OUTER JOIN {tableName} AS o ON e.EmployeeID = o.HeadID" +
                       " WHERE e.EmployeeID = @EmployeeID";


            var employees = await _connection.QueryAsync<OrganizationHead, EmployeeRole, OrganizationUnit, OrganizationHead>(
                sql,
                (
                    OrganizationHead employee,
                    EmployeeRole role,
                    OrganizationUnit unit) =>
                {
                    if (unit != null)
                    {
                        employee.HeadOf = unit;
                    }

                    employee.Role = role;

                    return employee;
                },
                new
                {
                    EmployeeID = employeeId,
                },
                _transaction,
                splitOn: "EmployeeID, RoleID, ID");

            return employees.FirstOrDefault();
        }

        public bool IsEmailTaken(string email)
        {
            string sql = @"
SELECT *
  FROM employees
 WHERE Email = @Email";
            var employees = _connection.Query<Employee>(sql, new {Email = email}, _transaction);
            return employees.Any();
        }
    }
}