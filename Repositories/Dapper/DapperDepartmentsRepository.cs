using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Companies.API.InputDTOs;
using Companies.API.Models;
using Companies.API.Repositories.Interfaces;
using Dapper;

namespace Companies.API.Repositories.Dapper
{
    public class DapperDepartmentsRepository : DapperOrganizationUnitRepository, IDepartmentsRepository
    {

        public DapperDepartmentsRepository(
            IDbConnection connection,
            IDbTransaction transaction,
            IEmployeesRepository employeesRepository) : base(connection, transaction, employeesRepository)
        {
        }

        public async Task<Department> GetDepartment(int departmentId)
        {
            string sql = @"
SELECT d.ID,
       d.Name,
       d.Code,
       e.EmployeeID,
       e.Degree,
       e.FirstName,
       e.LastName,
       e.PhoneExt,
       e.Email,
       r.RoleID,
       r.Name,
       e_dept.EmployeeID,
       e_dept.Degree,
       e_dept.FirstName,
       e_dept.LastName,
       e_dept.PhoneExt,
       e_dept.Email
  FROM departments AS d
LEFT OUTER JOIN employees AS e
    ON e.EmployeeID = d.HeadID
LEFT OUTER JOIN roles AS r
    ON r.RoleID = e.RoleID
LEFT OUTER JOIN departments_employees AS de
    ON de.DepartmentID = d.ID
LEFT OUTER JOIN employees AS e_dept
    ON e_dept.EmployeeID = de.EmployeeID
 WHERE d.ID = @DepartmentID
   ;";
            var departmentsDictionary = new Dictionary<int, Department>();
            var departments = await _connection.QueryAsync<Department, Employee, EmployeeRole, Employee, Department>(
                sql,
                (Department department, Employee boss, EmployeeRole bossRole,Employee employee) =>
                {
                    Department departmentEntry;
                    if (!departmentsDictionary.TryGetValue(department.ID, out departmentEntry))
                    {
                        departmentEntry = department;
                        if (boss != null)
                        {
                            boss.Role = bossRole;
                            departmentEntry.Head = boss;
                        }

                        departmentEntry.DepartmentEmployees = new List<Employee>();
                        departmentsDictionary.Add(departmentEntry.ID, departmentEntry);
                    }

                    // check for null because of left outer join
                    if (employee != null)
                    {
                        departmentEntry.DepartmentEmployees.Add(employee);
                    }

                    return departmentEntry;
                    
                },
                new 
                {
                    DepartmentID = departmentId,
                }, 
                _transaction,
                splitOn: "EmployeeID, RoleID, EmployeeID" 
            );
            
            return departments.FirstOrDefault();
        }

        public async Task<Employee> AddDepartmentHead(OrganizationUnitEmployeDto employee)
        {
            return await this.AddUnitHead(employee.unitId, employee.Employee, "departments");
        }

        public async Task<bool> AddDepartmentEmployee(int departmentId, Employee departmentEmployee)
        {
            string sql = @"
INSERT INTO departments_employees(DepartmentID, EmployeeID)
     VALUES (@DepartmentID, @EmployeID)";
            int affectedRows = await _connection.ExecuteAsync(sql, new
            {
                DepartmentID = departmentId,
                EmployeID = departmentEmployee.EmployeeID
            }, _transaction);

            return affectedRows > 0;
        }
    }
}