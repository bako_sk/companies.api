using System.Threading.Tasks;
using Companies.API.InputDTOs;
using Companies.API.Models;

namespace Companies.API.Repositories.Interfaces
{
    public interface IDepartmentsRepository
    {
        
        Task<Department> GetDepartment(int projectId);
        Task<Employee> AddDepartmentHead(OrganizationUnitEmployeDto employee);
        Task<bool> AddDepartmentEmployee(int departmentId, Employee departmentEmployee);
    }
}