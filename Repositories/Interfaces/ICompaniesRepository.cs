using System.Collections.Generic;
using System.Threading.Tasks;
using Companies.API.InputDTOs;
using Companies.API.Models;

namespace Companies.API.Repositories.Interfaces
{
    public interface ICompaniesRepository
    {
        Task<List<Company>> GetCompaniesWithoutDivisions();
        Task<Company> GetCompany(int id);
        Task<Employee> AddCeo(OrganizationUnitEmployeDto employee);
    }
}