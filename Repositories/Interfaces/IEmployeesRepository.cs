using System.Collections.Generic;
using System.Threading.Tasks;
using Companies.API.InputDTOs;
using Companies.API.Models;

namespace Companies.API.Repositories.Interfaces
{
    public interface IEmployeesRepository
    {
        Task<List<EmployeeRole>> GetAllRoles();
        Task<EmployeeRole> GetEmployeeRole(int employeeId);
        Task<DepartmentEmployee> GetDepartmentEmployee(int employeeId);
        Task<bool> UpdateEmployee(UpdateEmployeeDTO updatedEmployee);
        Task<Employee> AddEmployee(EmployeeDto employee);
        Task<bool> RemoveEmployee(int id);
        Task<OrganizationHead> GetOrganizationHead(int employeeId, string tableName);
        bool IsEmailTaken(string email);
    }
}