using System.Threading.Tasks;
using Companies.API.InputDTOs;
using Companies.API.Models;

namespace Companies.API.Repositories.Interfaces
{
    public interface IDivisionsRepository
    {
        Task<Division> GetDivision(int divisonId);
        Task<Employee> AddDivisonHead(OrganizationUnitEmployeDto employe);
        
    }
}