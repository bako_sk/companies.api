using System.Threading.Tasks;
using Companies.API.InputDTOs;
using Companies.API.Models;

namespace Companies.API.Repositories.Interfaces
{
    public interface IProjectRepository
    {
        Task<Project> GetProject(int projectId);
        Task<Employee> AddProjectHead(OrganizationUnitEmployeDto employee);
    }
}