using System.Threading.Tasks;
using Companies.API.InputDTOs;
using Companies.API.Models;
using Companies.API.Repositories.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Companies.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DepartmentsController : ControllerBase
    {

        private IDepartmentsRepository _repository;
        private IEmployeesRepository _employeesRepository;

        public DepartmentsController(IDepartmentsRepository repository,
                                     IEmployeesRepository employeesRepository)
        {
            _repository = repository;
            _employeesRepository = employeesRepository;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Department>> GetDepartment(int id)
        {
            Department department = await _repository.GetDepartment(id);
            if (department == null)
            {
                return NotFound();
            }
            else
            {
                return department;
            }
        }

        [HttpPost("add-employee")]
        public async Task<ActionResult<Employee>> AddDepartmentEmployee(OrganizationUnitEmployeDto employee)
        {
            Employee newEmployee = await _employeesRepository.AddEmployee(employee.Employee);
            if (newEmployee == null)
            {
                return BadRequest();
            }
            else
            {
                bool inserted = await _repository.AddDepartmentEmployee(employee.unitId, newEmployee);
                if (inserted)
                {
                    return newEmployee;
                }
                else
                {
                    return BadRequest();
                }
            }
        }
        [HttpPost("add-head")]
        public async Task<ActionResult<Employee>> AddDepartmentHead(OrganizationUnitEmployeDto employee)
        {
            Employee inserted = await _repository.AddDepartmentHead(employee);
            if (inserted == null)
            {
                return BadRequest();
            }
            else
            {
                    return inserted;
            }
        }
    }
}