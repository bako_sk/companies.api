using System.Threading.Tasks;
using Companies.API.InputDTOs;
using Companies.API.Models;
using Companies.API.Repositories.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Companies.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private IProjectRepository _repository;

        public ProjectsController(IProjectRepository repository)
        {
            _repository = repository;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Project>> GetProject(int id)
        {
            Project project = await _repository.GetProject(id);
            if (project == null)
            {
                return NotFound();
            }
            else
            {
                return project;
            }
        }
        [HttpPost("add-head")]
        public async Task<ActionResult<Employee>> AddProjectHead(OrganizationUnitEmployeDto employee)
        {
            Employee inserted = await _repository.AddProjectHead(employee);
            if (inserted == null)
            {
                return BadRequest();
            }
            else
            {
                    return inserted;
            }
        }
    }
}