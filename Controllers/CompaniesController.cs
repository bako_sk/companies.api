using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Companies.API.InputDTOs;
using Companies.API.Models;
using Companies.API.Repositories.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Companies.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompaniesController : ControllerBase
    {
        private readonly ICompaniesRepository _companiesRepository;
        private readonly IEmployeesRepository _employeesRepository;


        public CompaniesController(ICompaniesRepository companiesRepository, IEmployeesRepository employeesRepository)
        {
            _companiesRepository = companiesRepository;
            _employeesRepository = employeesRepository;
        }

        // return action result, if there can be various responses (404, 200 etc) in one call
        [HttpGet]
        public async Task<List<Company>> Companies()
        {
            return await _companiesRepository.GetCompaniesWithoutDivisions();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Company>> Company(int id)
        {
            Company found = await _companiesRepository.GetCompany(id);
            if (found == null)
            {
                return NotFound();
            }
            else
            {
                return found;
            }
        }
        [HttpPost("add-ceo")]
        public async Task<ActionResult<Employee>> AddCeo(OrganizationUnitEmployeDto employee)
        {
            Employee inserted = await _companiesRepository.AddCeo(employee);
            if (inserted == null)
            {
                return BadRequest();
            }
            else
            {
                    return inserted;
            }
        }
    }
}