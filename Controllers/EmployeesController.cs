using System.Collections.Generic;
using System.Threading.Tasks;
using Companies.API.InputDTOs;
using Companies.API.Models;
using Companies.API.Repositories.Interfaces;
using Microsoft.AspNetCore.Internal;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace Companies.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeesController : ControllerBase
    {
        private IEmployeesRepository _repository;

        public EmployeesController(IEmployeesRepository repository)
        {
            _repository = repository;
        }
        [HttpGet("roles")]
        public async Task<ActionResult<List<EmployeeRole>>> GetAllRoles()
        {
            return await _repository.GetAllRoles();
        }
        
        [HttpGet("{id}")]
        public async Task<ActionResult<Employee>> GetEmployee(int id)
        {
            EmployeeRole role = await _repository.GetEmployeeRole(id);
            if (role == null)
            {
                return BadRequest();
            }

            Employee employee = null;
            switch (role.Name)
            {
                case EmployeeRolesName.CEO:
                {
                    employee = await _repository.GetOrganizationHead(id, "companies");
                    break;
                }
                case EmployeeRolesName.DIVISON_HEAD:
                {
                    employee = await _repository.GetOrganizationHead(id, "divisions");
                    break;
                }
                case EmployeeRolesName.PROJECT_HEAD:
                {
                    employee = await _repository.GetOrganizationHead(id, "projects");
                    break;
                }
                case EmployeeRolesName.DEPARTMENT_HEAD:
                {
                    employee = await _repository.GetOrganizationHead(id, "departments");
                    break;
                }
                case EmployeeRolesName.DEPARTMENT_EMPLOYEE:
                {
                    employee = await _repository.GetDepartmentEmployee(id);
                    break;
                }
            }
            if (employee == null)
            {
                return NotFound();
            }
            else
            {

                return employee;
            }
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<bool>> RemoveEmployee(int id)
        {

            bool removed = await _repository.RemoveEmployee(id);
            if (removed)
            {
                return removed;
            }
            else
            {
                return NotFound();
            }
        }
       [HttpPut("{id}")]
       public async Task<IActionResult> UpdateEmployee(UpdateEmployeeDTO employeeDto)
       {
           bool updated = await _repository.UpdateEmployee(employeeDto);
           if (updated)
           {
               return NoContent();
           }
           else
           {
               return UnprocessableEntity();
           }
       }
       // only test!!!
       //[HttpPost]
       //public async Task<ActionResult<Employee>> AddEmployee(EmployeeDto employeeDto)
       //{
       //    Employee inserted = await _repository.AddEmployee(employeeDto);
       //    if (inserted == null)
       //    {
       //        return UnprocessableEntity();
       //    }
       //    else
       //    {
       //        return inserted;
       //    }
       //}
    }
}