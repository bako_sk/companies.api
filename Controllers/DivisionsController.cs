using System.Threading.Tasks;
using Companies.API.InputDTOs;
using Companies.API.Models;
using Companies.API.Repositories.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Companies.API.Controllers
{
    
    [Route("api/[controller]")]
    [ApiController]
    public class DivisionsController : ControllerBase
    {
        private IDivisionsRepository _repository;

        public DivisionsController(IDivisionsRepository repository)
        {
            _repository = repository;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Division>> GetDivision(int id)
        {
            Division division = await _repository.GetDivision(id);
            if (division == null)
            {
                return NotFound();
            }
            else
            {
                return division;
            }
        }
        [HttpPost("add-head")]
        public async Task<ActionResult<Employee>> AddDivisionHead(OrganizationUnitEmployeDto employee)
        {
            Employee inserted = await _repository.AddDivisonHead(employee);
            if (inserted == null)
            {
                return BadRequest();
            }
            else
            {
                    return inserted;
            }
        }
    }
}