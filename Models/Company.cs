using System.Collections.Generic;

namespace Companies.API.Models
{
    public class Company : OrganizationUnit
    {
        public List<OrganizationUnit> Divisions { get; set; }

        public override void AddSubUnit(OrganizationUnit unit)
        {
            if (this.Divisions == null)
            {
                this.Divisions = new List<OrganizationUnit>();
            }
            this.Divisions.Add(unit);
        }
    }
}