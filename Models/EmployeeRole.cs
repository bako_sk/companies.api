namespace Companies.API.Models
{
    public class EmployeeRole
    {
        public int RoleId { get; set; }
        public string Name { get; set; }
    }
}