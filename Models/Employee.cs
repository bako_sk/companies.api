using System.Collections.Generic;

namespace Companies.API.Models
{
    public class Employee
    {

        public int EmployeeID { get; set; }
        public string Degree { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneExt { get; set; }
        public string Email { get; set; }
        public EmployeeRole Role { get; set; }
    }
}