using System.Collections.Generic;

namespace Companies.API.Models
{
    public class DepartmentEmployee: Employee
    {
        
        public List<Department> Departments { get; set; }
    }
}