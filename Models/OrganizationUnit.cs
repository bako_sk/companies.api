using Microsoft.IdentityModel.Protocols.WsFederation;

namespace Companies.API.Models
{
    public class OrganizationUnit
    {
        public int ID { get; set; }
        public string Name{ get; set; }
        public string Code { get; set; }
        public Employee Head { get; set; }

        public virtual void AddSubUnit(OrganizationUnit unit)
        {
        }
    }
}