using System.Collections.Generic;

namespace Companies.API.Models
{
    public class Division : OrganizationUnit
    {
        public Company Company { get; set; }
        public List<OrganizationUnit> Projects { get; set; }

        public override void AddSubUnit(OrganizationUnit unit)
        {
            if (this.Projects == null)
            {
                this.Projects = new List<OrganizationUnit>();
            }
            this.Projects.Add(unit);
        }
    }
}