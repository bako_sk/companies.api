using System.Collections.Generic;

namespace Companies.API.Models
{
    public class Project : OrganizationUnit
    {
        public Division Division { get; set; }
        public List<OrganizationUnit> Departments { get; set; }

        public override void AddSubUnit(OrganizationUnit unit)
        {
            if (this.Departments == null)
            {
                this.Departments = new List<OrganizationUnit>();
            }

            this.Departments.Add(unit);
        }
    }
}