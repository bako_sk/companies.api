using System.Collections.Generic;

namespace Companies.API.Models
{
    public class Department : OrganizationUnit
    {
        public Project Project { get; set; }
        public List<Employee> DepartmentEmployees { get; set; }
    }
}