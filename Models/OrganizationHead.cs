namespace Companies.API.Models
{
    public class OrganizationHead : Employee
    {
        public OrganizationUnit HeadOf { get; set; }
    }
}