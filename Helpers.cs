using Microsoft.EntityFrameworkCore.Internal;

namespace Companies.API
{
    public class Helpers
    {
        public static bool IsTableNameCorrectUnitTable(string tableName)
        {
            
            // ugly hack - faster than checking DB
            string[] names = {"companies", "divisions", "projects", "departments" };
            int pos = names.IndexOf(tableName);
            return pos > -1;
        }
    }
}