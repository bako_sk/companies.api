using System;
using System.Collections.Generic;
using System.Reflection;
using Companies.API.Models;
using Dapper;

namespace Companies.API
{
    public class DapperCustomMapper
    {
        public static void RegisertCustomMappings()
        {
            // created using example on medium.com/dapper-net/custom-columns-mapping-1cd45dfd51d6
            // ??? try to use fluent mapping instead???
            
            //dictionary with cfg of how to mapp DB id columns to Id property of organization unit
            Dictionary<string, string> columnMaps = new Dictionary<string, string>()
            {
                // col => property
                {"CompanyID", "Id"},
                {"DivisionID", "Id"},
                {"ProjectID", "Id"},
                {"DepartmentID", "Id"}
            };

            var mapper = new Func<Type, string, PropertyInfo>((type, columnName) =>
            {
                if (columnMaps.ContainsKey(columnName))
                {
                    return type.GetProperty((columnMaps[columnName]));
                }
                else
                {
                    return type.GetProperty(columnName);
                }
            });

            var companyMap = new CustomPropertyTypeMap(
                typeof(Company),
                (type, columnName) => mapper(type, columnName)
            );
            var divisionMap = new CustomPropertyTypeMap(
                typeof(Division),
                (type, columnName) => mapper(type, columnName)
            );
            var projectMap = new CustomPropertyTypeMap(
                typeof(Project),
                (type, columnName) => mapper(type, columnName)
            );
            var departmentMap = new CustomPropertyTypeMap(
                typeof(Department),
                (type, columnName) => mapper(type, columnName)
            );
            
            SqlMapper.SetTypeMap(typeof(Company), companyMap);
            SqlMapper.SetTypeMap(typeof(Division), divisionMap);
            SqlMapper.SetTypeMap(typeof(Project), projectMap);
            SqlMapper.SetTypeMap(typeof(Department), departmentMap);
        }
    }
}